# Open Door -- Web -- TUCTF2019

[Source](https://gitlab.com/tuctf2019/web/open_door)

## Chal Info

Desc: `This challenge is an open door; show us you know how to find the key.`

Hints:

* How would you view the source code of a file?

Flag: `TUCTF{f1r5t_fl46_345135t_fl46}`

## Deployment

[Docker Hub](https://hub.docker.com/r/asciioverflow/open_door)

Ports: 80

Example usage:

```bash
docker run -d -p 127.0.0.1:8080:80 asciioverflow/open_door:tuctf2019
```
